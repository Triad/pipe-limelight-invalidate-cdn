'use strict'

const Purge = require('../lib/client').Purge

const USER = process.env.LL_USER
const API_KEY = process.env.LL_KEY

const REQUEST_BODY = {
  patterns:[
    {
      pattern: '*/test/*',
      evict: false,
      exact: false,
      incqs: true
    }
  ],
  email: {
      subject: 'TEST PURGE RESULTS',
      to: 'nathan@triadinteractive.com'
  },
  callback: {
      url: 'http://test.example.com/my_callback.php'
  },
  notes: 'TESTING SMART PURGE API'
}

const client = new Purge('triad', {
  user: USER,
  apiKey: API_KEY
})

client.create(REQUEST_BODY).then((response) => {
  console.info(JSON.stringify(response, null, 2))
}, (error) => {
  console.error(error)
})
