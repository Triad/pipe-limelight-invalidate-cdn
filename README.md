# Bitbucket Pipelines Pipe: Limelight CDN Purge

Purge your Limelight CDN endpoints using the SmartPurge API.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:  


```yaml
- pipe: triadinteractive/pipe-limelight-invalidate-cdn
  variables:
    SHORTNAME: "<string>"
    USERNAME: "<string>"
    API_KEY: "<string>"
    PATTERNS: "<string>"
    NOTES: "<string>"
    # EMAIL_TO: "<string>" # Optional
    # EMAIL_SUBJECT: "<string>" # Optional
    # DEBUG: "<string>" # Optional
```

## Variables

| Variable         | Usage                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SHORTNAME (\*)   | The short name (or account name) that Limelight uses to identify your accoutn.                                                                                                                                                                                                                                                                                                                                 |
| USERNAME (\*)    | The Limelight Control user account with which to request a CDN purge.                                                                                                                                                                                                                                                                                                                                |
| API_KEY (\*)     | Available in the Limelight Control web interface.                                                                                                                                                                                                                                                                                                                                       |
| PATTERNS (\*)    | Examples given below. See the Limelight SmartPurge Rest API documentation for more info.                                                                                                                                                                                                                                                                                                                          |
| NOTES            | Human-readable notes to be attached to the request. See the Limelight SmartPurge Rest API documentation for more info. |
| EMAIL_TO         | Email address(es) to send status updates to. If omitted, email is not sent.                                                                                                                                                                                                                    |
| EMAIL_SUBJECT    | Subject of the status update emails.                                                                                                                                                                                                                                                                                                                  |
| DEBUG            | Turn on extra debug information. See [NPM Debug module](https://www.npmjs.com/package/debug) for more info.                                                                                                                                                                                                                                                                                                                        |

_(\*) = required variable._

## Details

Limelight Control CDN can be invalidated or purged using their REST API. This pipe helps simplify the process of doing this.

## Prerequisites

- If you want to use the default behaviour for using the configured SSH key and known hosts file, you must have configured
  the SSH private key and known_hosts to be used for the RSYNC pipe in your Pipelines settings
  (see [docs](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html))

## Examples

### Basic example:

```yaml
script:
  - pipe: triadinteractive/pipe-limelight-invalidate-cdn
    variables:
      SHORTNAME: "your-ll-acct"
      USERNAME: "ll-user"
      API_KEY: "SECRET KEY GOES HERE"
      PATTERNS: '*/dir1/*,*/dir2/*'
      NOTES: "Note to help me remember what spawned this"
      EMAIL_TO: "me@domain.com"
      EMAIL_SUBJECT: "CDN Purge Update"
```

### Advanced examples:

Example with JSON data for more advanced pattern options. See Limelight SmartPurge REST API documentation for more info. Note: Skipped fields under each pattern will be populated with default values.

```yaml
script:
  - pipe: triadinteractive/pipe-limelight-invalidate-cdn
    variables:
      SHORTNAME: "your-ll-acct"
      USERNAME: "ll-user"
      API_KEY: "SECRET KEY GOES HERE"
      PATTERNS: '[{"pattern": "*/dir1/*"}, {"pattern": "*/exact-example.html", "exact": true}, {"pattern": "*/delete-instead/*", "evict": true}]'
      NOTES: "Note to help me remember what spawned this"
      EMAIL_TO: "me@domain.com"
      EMAIL_SUBJECT: "CDN Purge Update"
```

## Support

If you'd like help with this pipe, or you have an issue or feature request, [create a ticket](https://bitbucket.org/Triad/pipe-limelight-invalidate-cdn/issues/new).
