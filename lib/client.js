const defaults = require("lodash/defaults");
const API = require("./api");

function Purge(cfg) {
  cfg = defaults(cfg || {}, {
    protocol: "https",
    host: "purge.llnw.com",
    name: "purge",
    version: 1,

    shortname: null,
    user: null,
    apiKey: null,
    debug: false,
    dryRun: false
  });

  const client = new API(cfg);

  Object.defineProperties(this, {
    client: {
      value: client
    }
  });
}

Purge.prototype = {
  /**
   * Adds a new purge request to the queue
   * @param  {JSON} data Details about the purge request
   * @return {object} Returned from request library
   */
  create(data) {
    return this.client.request({
      method: "POST",
      endpoint: "requests",
      data: JSON.stringify(data)
    });
  },

  /**
   * Get the details of a previously-submitted purge request using its ID
   * @param  {string} id Identifier of existing purge request
   * @return {[type]}    [description]
   */
  details(id) {
    return this.client.request({
      endpoint: `requests/${id}`
    });
  },

  /**
   * Gets the details of previously-submitted purge requests
   * @return {object} Request object
   */
  list(cfg) {
    const endpoint = "requests";
    const qs = defaults({}, cfg || {}, {
      // start_ts: undefined,
      // end_ts: undefined,
      limit: 50,
      order: "desc"
    });

    return this.client.request({ endpoint, qs });
  },

  /**
   * Translates public exact URLs into corresponding origin URLs
   * @return {object} Request object
   */
  translate() {
    return this.client.request({
      endpoint: "translate"
    });
  }
};

module.exports.Purge = Purge;
