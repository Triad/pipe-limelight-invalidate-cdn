FROM node:12.13.1-alpine3.10

WORKDIR /opt/triad/pipe

COPY package.json package-lock.json ./
RUN npm install -p && rm -rf ~/.npm

COPY bin/ ./bin/
COPY lib/ ./lib/

CMD [ "/opt/triad/pipe/bin/purgecdn" ]